package com.example.alcchallenge.util;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferences {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;
    int PRIVATE_MODE = 0;

    private final String PREF_NAME = "ALC4";

    private final String track = "Track", country = "Country", email = "Email", phoneNumber = "Phone", slackUser = "slack";

    public SharedPreferences.Editor getEditor() {
        return editor;
    }

    public AppPreferences(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public String getTrack() {
        return pref.getString(track, "");
    }

    public void setTrack(String track) {
        editor.putString(this.track, track);
        editor.commit();
    }

    public String getCountry() {
        return pref.getString(country, "");
    }

    public void setCountry(String country) {
        editor.putString(this.country, country);
        editor.commit();
    }

    public String getEmail() {
        return pref.getString(email, "");
    }

    public void setEmail(String email) {
        editor.putString(this.email, email);
        editor.commit();
    }

    public String getPhoneNumber() {
        return pref.getString(phoneNumber, "");
    }

    public void setPhoneNumber(String phoneNumber) {
        editor.putString(this.phoneNumber, phoneNumber);
    }

    public String getSlackUser() {
        return pref.getString(slackUser, "");
    }

    public void setSlackUser(String slackUser) {
        editor.putString(this.slackUser, slackUser);
        editor.commit();
    }
}
