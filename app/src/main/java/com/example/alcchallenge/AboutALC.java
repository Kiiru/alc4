package com.example.alcchallenge;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.os.Bundle;
import android.util.Log;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.alcchallenge.util.AppPreferences;
import com.example.alcchallenge.util.Configs;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import static android.provider.ContactsContract.CommonDataKinds.Website.URL;

public class AboutALC extends AppCompatActivity {

    private WebView abt_alc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_alc);

        ActionBar ab = getSupportActionBar();
        ab.setTitle("About ALC");

        abt_alc = (WebView) findViewById(R.id.wv_alc4_webview);

        abt_alc.loadUrl(Configs.ALC);
        abt_alc.setWebViewClient(new MyWebViewClient());
        //abt_alc.clearSslPreferences();
        //abt_alc.
    }

    private class MyWebViewClient extends WebViewClient {
        public TrustManagerFactory tmf = null;

        private void initTrustStore() throws
                java.security.cert.CertificateException, FileNotFoundException,
                IOException, KeyStoreException, NoSuchAlgorithmException {

            // Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore trustedKeyStore = KeyStore.getInstance(keyStoreType);
            trustedKeyStore.load(null, null);

            CertificateFactory cf = CertificateFactory.getInstance("X.509");

            InputStream caInput = new BufferedInputStream(
                    getResources().getAssets().open("ca.crt"));
            Certificate ca;
            try {
                ca = cf.generateCertificate(caInput);
                Log.d(AboutALC.class.getSimpleName(), "ca-root DN=" + ((X509Certificate) ca).getSubjectDN());
            }
            finally {
                caInput.close();
            }
            trustedKeyStore.setCertificateEntry("ca", ca);

            // Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(trustedKeyStore);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (Uri.parse(url).getHost().equals(URL)) {
                // This is your web site, so do not override; let the WebView to load the page
                return false;
            }
            // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);

            // this will ignore the Ssl error and will go forward to your site
            //handler.proceed();
            //handler.` `
            Log.d(AboutALC.class.getSimpleName(), "onReceivedSslError");
            boolean passVerify = false;

            try {
                initTrustStore();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (CertificateException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            }


            if(error.getPrimaryError() == SslError.SSL_UNTRUSTED){
                SslCertificate cert = error.getCertificate();
                String subjectDN = cert.getIssuedTo().getDName();
                Log.d(AboutALC.class.getSimpleName(), "subjectDN: "+subjectDN);
                try{
                    Field f = cert.getClass().getDeclaredField("mX509Certificate");
                    f.setAccessible(true);
                    X509Certificate x509 = (X509Certificate)f.get(cert);

                    X509Certificate[] chain = {x509};
                    for (TrustManager trustManager: tmf.getTrustManagers()) {
                        if (trustManager instanceof X509TrustManager) {
                            X509TrustManager x509TrustManager = (X509TrustManager)trustManager;
                            try{
                                x509TrustManager.checkServerTrusted(chain, "generic");
                                passVerify = true;break;
                            }catch(Exception e){
                                Log.e(AboutALC.class.getSimpleName(), "verify trustManager failed", e);
                                passVerify = false;
                            }
                        }
                    }
                    Log.d(AboutALC.class.getSimpleName(), "passVerify: "+passVerify);
                }catch(Exception e){
                    Log.e(AboutALC.class.getSimpleName(), "verify cert fail", e);
                }
            }
            if(passVerify == true)handler.proceed();
            else handler.cancel();
        }
    }
}
