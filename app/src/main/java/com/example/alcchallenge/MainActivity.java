package com.example.alcchallenge;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.alcchallenge.util.AppPreferences;
import com.example.alcchallenge.util.ConnectionDetector;

public class MainActivity extends AppCompatActivity {

    private AppPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar ab = getSupportActionBar();
        ab.setTitle("ALC 4 Phase 1");
        prefs = new AppPreferences(MainActivity.this);

        prefs.setTrack("Android");
        prefs.setCountry("Kenya");
        prefs.setEmail("mwangi.ramson@gmail.com");
        prefs.setPhoneNumber("254714224569");
        prefs.setSlackUser("@ramsonmwangi");
    }

    public void loadAbout(View view) {
        ConnectionDetector cd = new ConnectionDetector(this);
        if(cd.isConnectingToInternet()){
            Intent intent = new Intent(this, AboutALC.class);
            startActivity(intent);
        } else{
            Toast.makeText(getApplicationContext(), "Make sure you are connected to internet", Toast.LENGTH_LONG).show();
        }

    }

    public void showProfile(View view) {
        Intent intent = new Intent(this, Profile.class);
        startActivity(intent);
    }
}
