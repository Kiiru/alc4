package com.example.alcchallenge;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.alcchallenge.util.AppPreferences;

public class Profile extends AppCompatActivity {

    private TextView track, country, email, phone, slackU;
    private AppPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        ActionBar ab = getSupportActionBar();
        ab.setTitle("My Profile");
        prefs = new AppPreferences(Profile.this);
        track = (TextView) findViewById(R.id.txt_track_value);
        country = (TextView) findViewById(R.id.txt_country_value);
        email = (TextView) findViewById(R.id.txt_email_value);
        phone = (TextView) findViewById(R.id.txt_phone_value);
        slackU = (TextView) findViewById(R.id.txt_slack_user_value);

        prefs = new AppPreferences(Profile.this);

        track.setText(prefs.getTrack());
        country.setText(prefs.getCountry());
        email.setText(prefs.getEmail());
        phone.setText(prefs.getPhoneNumber());
        slackU.setText(prefs.getSlackUser());

        // set image
    }
}
